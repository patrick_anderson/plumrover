# Plum Rover

I put this together iteratively, using the tests. I started with the WebApi project, something which I rather regret as I suspect it wasn't necessary and pushed my timeline out. I've left it in.

I've put tests only around the API, I haven't split the positional logic out to its own library as I'm making the assumption it will only be accessed via the WebApi. As this is the only entry point, tests are around the WebApi only.

## SOLID principles

I haven't really shown much SOLID design here, the reason is simplicity. I briefly started to think about have the Obstacle array as a dependency for a class that I would intantiate but then came to the conclusion that making it more complex as a way of showing IOC maybe making the whole thing look forced.

## Feedback

It maybe worth clarifying what API means, the term has rather morphed into a WebAPI and I was working under this assumption, probably mistakenly.

'non-verbal code' - I'm not sure what this means

Given more time I'd look into making the positioning code more concise, it seems verbose to me.

2 hours, no this took me longer, some of the time used was due to my rusty relationship with Visual Studio, I've been using Visual Studio Code mostly in the recent past
