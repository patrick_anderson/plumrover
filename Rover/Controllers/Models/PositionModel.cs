﻿using System;

namespace Rover.Controllers
{
    public class PositionModel
    {
        public int? X { get; set; }
        public int? Y { get; set; }
        public string Facing { get; set; }

        public IResult<Position> ParseAsPosition()
        {
            Direction facing;
            if (Enum.TryParse(this.Facing, true, out facing))
            {
                if (this.X != null && this.Y != null)
                {
                    return new IResult<Position>(new Position((int)this.X, (int)this.Y, facing));
                }
            }
            return new IResult<Position>();
        }
    }
}
