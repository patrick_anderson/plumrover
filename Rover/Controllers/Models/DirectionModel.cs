﻿using System;
using System.Collections.Generic;

namespace Rover.Controllers
{
    public class DirectionModel
    {
        public string Direction { get; set; }
        public PositionModel InitialPosition { get; set; }

        public IResult<DirectionalCommand[]> ParseDirection()
        {
            var directionCharArray = this.Direction.ToCharArray();
            List<DirectionalCommand> directions = new List<DirectionalCommand>();

            foreach (Char c in directionCharArray)
            {
                DirectionalCommand result;
                if (!Enum.TryParse(c.ToString(), true, out result))
                    return new IResult<DirectionalCommand[]>();
                directions.Add(result);


            }

            return new IResult<DirectionalCommand[]>(directions.ToArray());
        }
    }
}
