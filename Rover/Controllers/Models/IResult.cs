﻿namespace Rover.Controllers
{
    public class IResult<T>
    {

        public IResult()
        {
            IsSuccess = false;
        }
        public IResult(T obj) : this(obj, true)
        {

        }

        public IResult(T obj, bool isSuccess)
        {
            IsSuccess = isSuccess;
            Result = obj;
        }
        public bool IsSuccess { get; }
        public T Result { get; }
    }
}
