﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;

namespace Rover.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public partial class RoverController : ControllerBase
    {
        private readonly ILogger<RoverController> _logger;

        public RoverController(ILogger<RoverController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        public IActionResult Post([FromBody]DirectionModel directionModel)
        {
            var direction = directionModel.ParseDirection();
            if (!direction.IsSuccess)
            {
                return BadRequest($"Unable to parse Direction '{directionModel.Direction}'");
            }


            var position = directionModel.InitialPosition.ParseAsPosition();
            if(!position.IsSuccess)
            {
                return BadRequest($"Unable to parse InitialPosition '{JsonConvert.SerializeObject(directionModel.InitialPosition)}'");
            }

            return Ok(PositionalChanger.ChangePosition(position.Result,  direction.Result,new Obstacle[] { new Obstacle(6, 2) }));
        }
    }    
}
