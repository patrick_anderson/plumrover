﻿namespace Rover.Controllers
{
    public enum DirectionalCommand
    {
        F = 0,
        B = 1,
        L = 2,
        R = 3
    }
}
