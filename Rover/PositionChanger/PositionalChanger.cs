﻿using System;
using System.Collections.Generic;

namespace Rover.Controllers
{
    public class PositionalChanger
    {
        private static int GetPositiveIntModulus(int value, int mod)
        {
            return ((value % mod) + mod) % mod;
        }

        private const int GridLength = 100;

        private static Dictionary<DirectionalCommand, int> GenerateBFLRDictionary(int back, int forward, int left, int right)
        {
            Dictionary<DirectionalCommand, int> directionDictionary = new Dictionary<DirectionalCommand, int>();
            directionDictionary.Add(DirectionalCommand.B, back);
            directionDictionary.Add(DirectionalCommand.F, forward);
            directionDictionary.Add(DirectionalCommand.L, left);
            directionDictionary.Add(DirectionalCommand.R, right);
            return directionDictionary;
        }

        private static int MoveX(DirectionalCommand directionalCommand, Direction facing)
        {
            Dictionary<Direction, Dictionary<DirectionalCommand, int>> directionalDictionary = new Dictionary<Direction, Dictionary<DirectionalCommand, int>>();
            directionalDictionary.Add(Direction.E, GenerateBFLRDictionary(-1, 1, 0, 0));
            directionalDictionary.Add(Direction.W, GenerateBFLRDictionary(1, -1, 0, 0));
            directionalDictionary.Add(Direction.S, GenerateBFLRDictionary(0, 0, 0, 0));
            directionalDictionary.Add(Direction.N, GenerateBFLRDictionary(0, 0, 0, 0));
            return directionalDictionary[facing][directionalCommand];
        }

        private static int MoveY(DirectionalCommand directionalCommand, Direction facing)
        {
            Dictionary<Direction, Dictionary<DirectionalCommand, int>> directionalDictionary = new Dictionary<Direction, Dictionary<DirectionalCommand, int>>();
            directionalDictionary.Add(Direction.N, GenerateBFLRDictionary(-1, 1, 0, 0));
            directionalDictionary.Add(Direction.S, GenerateBFLRDictionary(1, -1, 0, 0));
            directionalDictionary.Add(Direction.E, GenerateBFLRDictionary(0, 0, 0, 0));
            directionalDictionary.Add(Direction.W, GenerateBFLRDictionary(0, 0, 0, 0));
            return directionalDictionary[facing][directionalCommand];
        }

        private static Direction Turn(DirectionalCommand directionalCommand, Direction facing)
        {
            var directions = new Direction[] { Direction.N, Direction.E, Direction.S, Direction.W };
            var currentIndex = Array.IndexOf(directions, facing);
            Dictionary<DirectionalCommand, int> turnDirection = GenerateBFLRDictionary(0, 0, -1, 1);
            var newIndex = GetPositiveIntModulus((currentIndex + turnDirection[directionalCommand]), directions.Length);
            return directions[newIndex];
        }

        private static int WrapDetection(int position)
        {
            return GetPositiveIntModulus(position, GridLength + 1);
        }

        private static Position ChangePosition(Position position, DirectionalCommand directionalCommand)
        {
            var y = MoveY(directionalCommand, position.Facing);
            var x = MoveX(directionalCommand, position.Facing);
            var newY = WrapDetection(position.Y + y);
            var newX = WrapDetection(position.X + x);
            return new Position(newX, newY, Turn(directionalCommand, position.Facing));
        }

        /// <summary>
        /// Will iterate through the DirectionCommands and return a new Position
        /// </summary>
        public static PositionChangeResult ChangePosition(Position position, DirectionalCommand[] directionalCommands, Obstacle[] obstacles )
        {
            var lastPosition = position;
            foreach (DirectionalCommand directionalComman in directionalCommands)
            {
                var nextPosition = ChangePosition(lastPosition, directionalComman);
                bool isEncounteredObstacle = Obstacle.IsObstaclePosition(obstacles, nextPosition.X, nextPosition.Y);
                if (isEncounteredObstacle)
                {
                    return new PositionChangeResult(lastPosition, true);
                }
                lastPosition = nextPosition;
            }
            return new PositionChangeResult(lastPosition);
        }
    }
}
