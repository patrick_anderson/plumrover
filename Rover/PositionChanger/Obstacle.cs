﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rover.Controllers
{
    public class Obstacle
    {
        public Obstacle(int x, int y)
        {
            X = x;
            Y = y;
        }
        public int Y { get; }
        public int X { get; }

        public static bool IsObstaclePosition(Obstacle[] obstacles, int x, int y)
        {
            return obstacles.Any(obstacle => obstacle.X == x && obstacle.Y == y);
        }
    }
}
