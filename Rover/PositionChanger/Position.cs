﻿namespace Rover.Controllers
{
    public class Position
    {

        public Position(int x, int y, Direction facing)
        {
            X = x;
            Y = y;
            Facing = facing;
        }

        public int X { get; }
        public int Y { get; }
        public Direction Facing { get; }
    }
}
