﻿namespace Rover.Controllers
{
    public class PositionChangeResult
    {
        public PositionChangeResult(Position position)
        {
            Position = position;
            FoundObstacle = false;
        }
        public PositionChangeResult(Position position, bool foundObstacle)
        {
            Position = position;
            FoundObstacle = foundObstacle;
        }

        public Position Position { get; }
        public bool FoundObstacle { get; }
    }
}
