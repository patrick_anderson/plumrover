﻿//using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using NUnit.Framework;
using Rover.Controllers;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace Rover.Tests
{
    [TestFixture]
    public class RoverTests
    {
        public System.Net.Http.HttpClient _client;
        public PositionModel _currentPosition;

        [OneTimeSetUp]
        public void Setup()
        {
            APIWebApplicationFactory factory = new APIWebApplicationFactory();
            _client = factory.CreateClient();
            _currentPosition = new PositionModel() { X = 0, Y = 0, Facing = "N" };
        }


        [Test]
        public async Task Post_With_Incorrect_Position_Arg_Should_Return_400()
        {
            var response = await _client.PostAsync("/rover", JsonContent.Create(new
            {
                Direction = "F",
                InitialPosition = new { X = 12 }
            }));


            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            Assert.That(await response.Content.ReadAsStringAsync(), Is.EqualTo("Unable to parse InitialPosition '{\"X\":12,\"Y\":null,\"Facing\":null}'"));
        }

        [Test]
        public async Task Post_With_Incorrect_Directional_Arg_Should_Return_400()
        {
            var response = await _client.PostAsync("/rover", JsonContent.Create(new
            {
                Direction = "T",
                InitialPosition = _currentPosition
            }));


            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            Assert.That(await response.Content.ReadAsStringAsync(), Is.EqualTo("Unable to parse Direction 'T'"));
        }

        [Test]
        public async Task Post_With_One_Incorrect_Directional_Arg_Should_Return_400()
        {
            var response = await _client.PostAsync("/rover", JsonContent.Create(new
            {
                Direction = "FBxLR",
                InitialPosition = _currentPosition
            }));


            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            Assert.That(await response.Content.ReadAsStringAsync(), Is.EqualTo("Unable to parse Direction 'FBxLR'"));
        }

        [TestCase("F", 0, 1,false, Direction.N,TestName ="F")]
        [TestCase("FF", 0, 2, false,Direction.N,TestName ="FF")]
        [TestCase("FFFBB", 0, 1,false, Direction.N,TestName ="FFFBB")]
        [TestCase("FFFFBB", 0, 2, false,Direction.N,TestName ="FFFFBB")]
        [TestCase("FFRFF", 2, 2,false, Direction.E,TestName ="FFRFF")]
        [TestCase("FRFFLFFL", 2, 3,false, Direction.W,TestName ="FRFFLFFL")]
        [TestCase("FLF", 100, 1,false, Direction.W,TestName ="FLF")]
        [TestCase("B", 0, 100,false, Direction.N,TestName ="B")]
        [TestCase("BRRRF", 100, 100,false, Direction.W,TestName ="BRRRF")]
        [TestCase("RFFFFFFFLFF", 7, 2,false, Direction.N,TestName ="RFFFFFFFLFF")]
        [TestCase("FFRFFFFFFFL", 5, 2,true, Direction.E,TestName ="FFRFFFFFFFL")]
        public async Task Post_(string directionalHint, int resultingX, int resultY,bool isObstacle, Direction resultFacing)
        {
            var response = await _client.PostAsync("/rover", JsonContent.Create(new
            {
                Direction = directionalHint,
                InitialPosition = _currentPosition
            }));


            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

            var result = await response.Content.ReadFromJsonAsync<Model>();


            Assert.That(result.Position.Facing, Is.EqualTo(resultFacing));
            Assert.That(result.Position.X, Is.EqualTo(resultingX));
            Assert.That(result.Position.Y, Is.EqualTo(resultY));
            Assert.That(result.FoundObstacle, Is.EqualTo(isObstacle));
        }
    }

    public class Model
    {
        public Position Position { get;set; }
        public bool FoundObstacle { get;set; }
    }
}
